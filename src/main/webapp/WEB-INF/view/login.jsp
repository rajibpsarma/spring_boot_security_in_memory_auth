<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!doctype HTML>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Spring Security Demo</title>
		<link rel="stylesheet" href="css/bootstrap.min.css">
	</head>
	
	<body>
		<form:form method="post" action="/authenticate" name="loginForm">
			<div class="container border rounded bg-light mt-2">
				<div class="row">
					<div class="col-12"><h5 class="mb-0">Spring Security Demo</h5>
					Uses In-memory authentication
					<hr class="border-primary"></div>
				</div>
				<div class="row">
					<div class="col-12">Please login to continue ! </div>
				</div>
				<div class="row mb-3">
					<div class="col-12 text-danger">${errorMsg} ${logoutMsg}&nbsp;</div>
				</div>				
				<div class="row">
					<div class="col-3"><label>User Name</label></div>
					<div class="col-3"><input type="text" name="username"></div>
				</div>
				<div class="row mb-2">
					<div class="col-3"><label>Password</label></div>
					<div class="col-3"><input type="password" name="password"></div>
				</div>		
				<div class="row mb-2">
					<div class="col-3"></div>
					<div class="col">
						<button onclick="login" class="btn btn-primary">Login</button>
						<input type="reset" class="btn border rounded" value="Clear">
					</div>
				</div>	
			</div>
		</form:form>
		<script>
			function login() {
				document.loginForm.submit();
			}
		</script>
	</body>
</html>