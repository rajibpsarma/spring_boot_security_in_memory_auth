package org.bitbucket.rajibpsarma.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Configuration
@EnableWebSecurity
public class AppConfig extends WebSecurityConfigurerAdapter {
	
	// In-memory authentication, expects a user "rajib" with password "rajib"
	//@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		UserBuilder users = User.withDefaultPasswordEncoder();
		
		auth.inMemoryAuthentication()
			.withUser(users.username("rajib").password("rajib").roles("ADMIN"));
	}
	
	// Security related configuration
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()	// Allows restricting access
			.antMatchers("/css/*", "/login").permitAll() // CSS files are allowed, used in login page
			.anyRequest().authenticated()	// All requests need authentication
			.and()
				.formLogin()	// Enable Form based authentication
				.loginPage("/login")	// The login page
				// No need to define "/authenticate" in controller, provided automatically by spring.
				// We do not write any code for authentication.				
				.loginProcessingUrl("/authenticate") // The URL to validate the credentials
				.defaultSuccessUrl("/home")
				.permitAll()
			.and()
				.logout().permitAll(); // Enable logout functionality, default url "/logout"
	}
}
