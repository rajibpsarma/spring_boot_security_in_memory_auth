package org.bitbucket.rajibpsarma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InMemoryAuthApp {

	public static void main(String[] args) {
		SpringApplication.run(InMemoryAuthApp.class, args);
	}

}
