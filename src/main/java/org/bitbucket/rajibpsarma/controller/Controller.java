package org.bitbucket.rajibpsarma.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@org.springframework.stereotype.Controller
public class Controller {

	@GetMapping("/login")
	public String showLoginPage(HttpServletRequest request, Model model) {
		// In case of an error, i.e. "/login?error", show an error message
		String error = request.getParameter("error");
		if(error != null) {
			String errorMsg = "Invalid Credentials !";
			model.addAttribute("errorMsg", errorMsg);
		}
		
		// In case of logout, i.e. "/login?logout", show proper message
		String logout = request.getParameter("logout");
		if(logout != null) {
			String logoutMsg = "You have been logged out !";
			model.addAttribute("logoutMsg", logoutMsg);
		}
		
		return "login";
	}

	@GetMapping("/home")
	public String showHomePage(Model model, Authentication auth) {
		// Get logged in user details
		// Another way
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String username = auth.getName();
		Object[] arr = auth.getAuthorities().toArray();
		StringBuffer buf = new StringBuffer("(");
		for(int i=0; i<arr.length; i++) {
			buf.append(arr[i].toString());
			if(i != (arr.length -1)) {
				buf.append(", ");
			} else {
				buf.append(")");
			}
		}
		model.addAttribute("userName", username);
		model.addAttribute("userRoles", buf.toString());
		return "home";
	}
}
