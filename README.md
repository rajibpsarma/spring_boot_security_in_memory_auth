# Spring Boot security using In-Memory Authentication

It's a demo app that uses a custom login page and Spring in-memory authentication to verify credentials.
Also, it uses logout functionality.

It uses JSP as view. Bootstrap is used to style the pages.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_boot_security_in_memory_auth.git
* cd spring_boot_security_in_memory_auth
* mvn clean spring-boot:run
* Invoke the app using "http://localhost:8080/"
* Enter invalid credentials xyz/xyz, check the error message.
* Enter valid credentials rajib/rajib, navigates to home page.
* Click on Logout, displays the login page with a logout message.


